//
//  WebsiteAPI.swift
//  RestaurantSingapore
//
//  Created by Preshen Naidoo on 2019/08/14.
//  Copyright © 2019 Preshen Naidoo. All rights reserved.
//



import Foundation

struct Coordinates {
    var latitude : Double
    var longitude : Double
}

struct RestaurantDetails {
    var name : String
    var address : String
    var postalcode : Int
    var image : String
    var Location : Coordinates
}

protocol WebApiDelegate {
    func RestaurantsFetched(_ restaurant: [RestaurantDetails])
}

class WebsiteAPI {
    
    private var delegate: WebApiDelegate?
    
    init(delegate: WebApiDelegate) {
        self.delegate = delegate
    }
    
    func sessionOpen(_ url: URL){
        let session = URLSession.shared
        var ListOfRestaurants = [RestaurantDetails]()
        
        session.dataTask(with: url) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    if let object = json as? [Any]{
                        for item in object as! [Dictionary<String,AnyObject>]{
                            let restaurantName = item["name"] as! String
                            let restaurantAddress = item["address"] as! String
                            let restaurantPostalcode = item["postalcode"] as! Int
                            let restaurantLatitude = item["lat"] as! Double
                            let restaurantLongitude = item["lng"] as! Double
                            let RestaurantDetailsFetched = (RestaurantDetails(name: restaurantName, address: restaurantAddress, postalcode: restaurantPostalcode, image: "" , Location: Coordinates(latitude: restaurantLatitude, longitude: restaurantLongitude)))
                            ListOfRestaurants.append(RestaurantDetailsFetched)
                        }
                        
                    }
                    else{
                        print("Invalid JSON")
                    }
                    
                    DispatchQueue.main.async {
                        self.delegate?.RestaurantsFetched(ListOfRestaurants)
                    }
                    
                }
                catch{
                    print(error)
                }
            }
        }.resume()
        
        
    }
    
}
