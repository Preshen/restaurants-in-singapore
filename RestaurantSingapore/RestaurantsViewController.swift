//
//  ViewController.swift
//  RestaurantSingapore
//
//  Created by Preshen Naidoo on 2019/08/14.
//  Copyright © 2019 Preshen Naidoo. All rights reserved.
//

import UIKit

var ListofRestaurants = [RestaurantDetails]()
var WebApi : WebsiteAPI?
let url = URL(string: "https://api.jael.ee/datasets/hawker")!

class RestaurantsViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
    
    private lazy var viewModel = RestaurantsViewModel()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListofRestaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cell")
        cell.textLabel?.text = ListofRestaurants[indexPath.row].name
        cell.detailTextLabel?.text = ListofRestaurants[indexPath.row].address
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        }else {
            cell.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectedRestaurant = ListofRestaurants[indexPath.row]
            performSegue(withIdentifier: "RestaurantDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RestaurantDetails" {
            if let destinationViewController = segue.destination as? RestaurantDetailViewController {
                destinationViewController.restaurantDetails = viewModel.selectedRestaurant
            }
        }
    }

    
    @IBOutlet weak var RestaurantTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RestaurantTable.delegate = self
        RestaurantTable.dataSource = self
        WebApi = WebsiteAPI(delegate: self)
        WebApi?.sessionOpen(url)
        
    }
    
    
    
}

extension RestaurantsViewController : WebApiDelegate {
    func RestaurantsFetched(_ restaurant: [RestaurantDetails]) {
        ListofRestaurants = restaurant
        RestaurantTable.reloadData()
    }
}
