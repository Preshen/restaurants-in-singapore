//
//  RestaurantDetailViewController.swift
//  RestaurantSingapore
//
//  Created by Preshen Naidoo on 2019/08/15.
//  Copyright © 2019 Preshen Naidoo. All rights reserved.
//

import UIKit
import MapKit

class RestaurantDetailViewController: UIViewController {

    @IBOutlet weak var AddressLabel: UILabel!
    var restaurantDetails : RestaurantDetails?
    @IBOutlet weak var MapOfRestaurant: MKMapView!
    var latitude : Double!
    var longitude : Double!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = restaurantDetails?.name
        self.AddressLabel.text = restaurantDetails?.address
        latitude = restaurantDetails?.Location.latitude
        longitude = restaurantDetails?.Location.longitude
        
        let location = CLLocationCoordinate2DMake(latitude!,longitude!)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        MapOfRestaurant.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = restaurantDetails?.name
        MapOfRestaurant.addAnnotation(annotation)
        
    }
    
    @IBAction func OpenMaps(_ sender: UIButton) {
        let regionDistance:CLLocationDistance = 5000
        let coordinates = CLLocationCoordinate2DMake(latitude!, longitude!)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
         let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
         ]
         let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
         let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(self.restaurantDetails?.name ?? "")"
        mapItem.openInMaps(launchOptions: options) }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
